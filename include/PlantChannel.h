/**************************************************************
 * Got the code for sonification from:
 * https://github.com/electricityforprogress/BiodataSonificationBreadboardKit/blob/9b1064cb4f23ed0b710c741ef8dff320ebb755cb/BiodataSonification_026_kit.ino
 **************************************************************/
#ifndef PLANTCHANNEL_H
#define PLANTCHANNEL_H
#include <Arduino.h>

#define SAMPLESIZE 10
#define ANALYSIZE 9
#define POLYPHONY 5

enum MIDIType {
    MIDI_NOTE = 144,
    MIDI_CONTROL = 176
};

typedef struct _MIDImessage { //build structure for Note and Control MIDImessages
  uint8_t type;
  uint8_t value;
  uint8_t velocity;
  long duration;
  long period;
  uint8_t channel;
} MIDImessage;


class PlantChannel
{
    public:
        PlantChannel(uint8_t input, uint8_t channel, void (*cb)(uint8_t, const MIDImessage&) = nullptr);
        ~PlantChannel();

        void setMIDISerial(HardwareSerial& midi);

        void update();
        void sample();
        void analyzeSample();
        uint32_t getCount();

        void setThreshold(int value, int min, int max);

        void printMIDIMessage(const MIDImessage& m);

    private:
        void setNote(int value, int velocity, long duration, int notechannel);
        int scaleNote(int note, int scale[], int root);
        int scaleSearch(int note, int scale[], int scalesize);
        void setControl(int type, int value, int velocity, long duration);
        void sendMIDI(MIDIType type, int channel, int data1, int data2);
        
        void checkControl();
        void checkNote();
        float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
        
    private:
        // I/O
        uint8_t input;

        HardwareSerial midi = HardwareSerial(1);

        uint8_t channel;
        unsigned long currentMillis = 0;

        // callback
        void (*callback)(uint8_t, const MIDImessage&) = nullptr;

        // sampling
        uint32_t count;
        volatile unsigned long microseconds;
        volatile unsigned long samples[SAMPLESIZE];
        volatile uint8_t index = 0;


        // bool QY8 = false;  //sends each note out chan 1-4, for use with General MIDI like Yamaha QY8 sequencer
        byte controlNumber = 80; //set to mappable control, low values may interfere with other soft synth controls!!
        byte controlVoltage = 1; //output PWM CV on controlLED, pin 17, PB3, digital 11 *lowpass filter

        // notes and scales
        //set scaled values, sorted array, first element scale length
        static const int scaleCount = 5;
        static const int scaleLen = 13; //maximum scale length plus 1 for 'used length'
        int currScale = 0; //current scale, default Chrom
        int scale[scaleCount][scaleLen] = {
            {12,1,2,3,4,5,6,7,8,9,10,11,12}, //Chromatic
            {7,1, 3, 5, 6, 8, 10, 12}, //Major
            {7,1, 3, 4, 6, 8, 9, 11}, //DiaMinor
            {7,1, 2, 2, 5, 6, 9, 11}, //Indian
            {7,1, 3, 4, 6, 8, 9, 11} //Minor
        };
        int root = 0; //initialize for root, pitch shifting

        int noteMin = 36; //C2  - keyboard note minimum
        int noteMax = 96; //C7  - keyboard note maximum

        float threshold = 1.7;   //2.3;  //change threshold multiplier
        float threshMin = 1.61; //scaling threshold min
        float threshMax = 3.71; //scaling threshold max
        float knobMin = 1;
        float knobMax = 1024;

        // MIDI
        MIDImessage noteArray[POLYPHONY]; //manage MIDImessage data as an array with size polyphony
        int noteIndex = 0;
        MIDImessage controlMessage; //manage MIDImessage data for Control Message (CV out)

};

#endif //PLANTCHANNEL_H