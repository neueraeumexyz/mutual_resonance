#mutual resonance

use_debug false
use_bpm 20
buffer(:foo, 48)
b = buffer(:foo)
puts b.duration

with_fx :sound_out, output: 3 do
  live_loop :plant1a do
    stop
    note  = sync"/midi:usb_midi_interface:0:2/control_change" # correct adress
    velocity, duration = sync"/midi:usb_midi_interface:0:2/control_change"
    z = note[0]
    y = note[1]
    with_fx :hpf, cutoff: 130 do
      use_synth :gnoise
      play z, attack: 2, release: 3, amp: y/40
      sleep 2
    end
  end
  
  live_loop :plant1b do
    stop
    note = sync"/midi:usb_midi_interface:0:2/control_change"
    velocity, duration = sync"/midi:usb_midi_interface:0:2/control_change"
    z = note[0]
    y = note[1]
    use_synth :sine
    #with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.2 do
    with_fx :flanger, phase: 6, phase_offset: 0.5, delay: 4 do
      play note, amp: velocity/300.0, release: duration/63.5, attack: [1, 0.5, 1.5].choose, env_curve: 1, cutoff: 80
      sleep 2
      # end
    end
  end
end




###
#
with_fx :sound_out, output: 4 do
  live_loop :plant2a do
    #stop
    note  = sync"/midi:usb_midi_interface:0:1/note_on"
    velocity, duration = sync"/midi:usb_midi_interface:0:1/control_change"
    z = note[0]
    y = note[1]
    use_synth :sine
    with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
      play note, amp: velocity/80.0, pan: rrand(-1, 1), env_curve: 1
      sleep 2
    end
  end
  
  
  live_loop :plant2b do
    #stop
    note = sync"/midi:usb_midi_interface:0:1/note_on"
    velocity, duration = sync"/midi:usb_midi_interface:0:1/control_change"
    z = note[0]
    y = note[1]
    use_synth :dark_ambience
    with_fx :flanger, mix: 0.7 do
      play choose([z, y]), attack: 5, release: 5, amp: y/30, room: 90, res: 0.9
      sleep 5
    end
  end
end


####

with_fx :sound_out, output: 5, amp: 3 do
  live_loop :plant3a do
    #stop
    note  = sync"/midi:usb_midi_interface:0:3/note_on"
    velocity, duration = sync"/midi:usb_midi_interface:0:3/control_change"
    z = note[0]
    y = note[1]
    use_synth :hollow
    #with_fx :bpf, centre: 80, res: 0.7 do
    #with_fx :wobble, phase: 0.8, cutoff_min: 60, cutoff_max: 120, res: 0.7, wave: 1, probability: 0.7, pre_amp: 0.5 do
    play note, amp: velocity/40.0, attack: 0.5, release: duration/63.5, pan: rrand(-1, 1)
    sleep [1, 0.5].choose
    #end
    #end
  end
  
  live_loop :plant3b do
    stop
    note  = sync"/midi:usb_midi_interface:0:3/note_on"
    velocity, duration = sync"/midi:usb_midi_interface:0:3/control_change"
    z = note[0]
    y = note[1]
    use_synth :sine
    # with_fx :bpf, centre: 130, res: 0.7 do
    play note, amp: velocity/40.0, attack: 0.5, sustain: duration/40.0, release: [0.1, 0.5, 1, 2].choose, pan: rrand(-1, 1)
    sleep [1, 1.5].choose #
    #end
  end
end



####
with_fx :sound_out, output: 6 do
  live_loop :plant4a do
    #stop
    note  = sync"/midi:usb_midi_interface:0:4/control_change"
    velocity, duration = sync"/midi:usb_midi_interface:0:4/control_change"
    z = note[0]
    y = note[1]
    with_fx :hpf, cutoff: 130 do
      use_synth :gnoise
      play z, attack: 2, release: 3, amp: y/20
      sleep 1
    end
  end
  
  
  live_loop :plant4b do
    #stop
    note  = sync"/midi:usb_midi_interface:0:4/control_change"
    velocity, duration = sync"/midi:usb_midi_interface:0:4/control_change"
    z = note[0]
    y = note[1]
    use_synth :pretty_bell
    #with_fx :rhpf, cutoff: 90, res: 0.3 do
    #with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.2 do
    play note, amp: velocity/200.0, pan: rrand(-1, 1)
    sleep 1 #duration/127.0
    #end
    #end
  end
end




















