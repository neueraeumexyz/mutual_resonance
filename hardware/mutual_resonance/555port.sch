EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Timer:NE555P U2
U 1 1 62B4E2B6
P 4750 2450
AR Path="/63239588/62B4E2B6" Ref="U2"  Part="1" 
AR Path="/6323C0DC/62B4E2B6" Ref="U3"  Part="1" 
AR Path="/6323F750/62B4E2B6" Ref="U4"  Part="1" 
AR Path="/632405C4/62B4E2B6" Ref="U5"  Part="1" 
F 0 "U2" H 4750 3031 50  0000 C CNN
F 1 "NE555P" H 4750 2940 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5400 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 5600 2050 50  0001 C CNN
	1    4750 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR028
U 1 1 62B4F8A8
P 4750 1800
AR Path="/63239588/62B4F8A8" Ref="#PWR028"  Part="1" 
AR Path="/6323C0DC/62B4F8A8" Ref="#PWR037"  Part="1" 
AR Path="/6323F750/62B4F8A8" Ref="#PWR046"  Part="1" 
AR Path="/632405C4/62B4F8A8" Ref="#PWR055"  Part="1" 
F 0 "#PWR028" H 4750 1650 50  0001 C CNN
F 1 "+3V3" H 4765 1973 50  0000 C CNN
F 2 "" H 4750 1800 50  0001 C CNN
F 3 "" H 4750 1800 50  0001 C CNN
	1    4750 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 62B5022C
P 4750 3100
AR Path="/63239588/62B5022C" Ref="#PWR029"  Part="1" 
AR Path="/6323C0DC/62B5022C" Ref="#PWR038"  Part="1" 
AR Path="/6323F750/62B5022C" Ref="#PWR047"  Part="1" 
AR Path="/632405C4/62B5022C" Ref="#PWR056"  Part="1" 
F 0 "#PWR029" H 4750 2850 50  0001 C CNN
F 1 "GND" H 4755 2927 50  0000 C CNN
F 2 "" H 4750 3100 50  0001 C CNN
F 3 "" H 4750 3100 50  0001 C CNN
	1    4750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3100 4750 2850
Wire Wire Line
	4250 2250 3800 2250
$Comp
L Device:CP C1
U 1 1 62B53134
P 3350 2450
AR Path="/63239588/62B53134" Ref="C1"  Part="1" 
AR Path="/6323C0DC/62B53134" Ref="C6"  Part="1" 
AR Path="/6323F750/62B53134" Ref="C11"  Part="1" 
AR Path="/632405C4/62B53134" Ref="C16"  Part="1" 
F 0 "C1" V 3605 2450 50  0000 C CNN
F 1 "47uF" V 3514 2450 50  0000 C CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3388 2300 50  0001 C CNN
F 3 "~" H 3350 2450 50  0001 C CNN
	1    3350 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR027
U 1 1 62B54B29
P 3200 2250
AR Path="/63239588/62B54B29" Ref="#PWR027"  Part="1" 
AR Path="/6323C0DC/62B54B29" Ref="#PWR036"  Part="1" 
AR Path="/6323F750/62B54B29" Ref="#PWR045"  Part="1" 
AR Path="/632405C4/62B54B29" Ref="#PWR054"  Part="1" 
F 0 "#PWR027" H 3200 2100 50  0001 C CNN
F 1 "+3V3" H 3215 2423 50  0000 C CNN
F 2 "" H 3200 2250 50  0001 C CNN
F 3 "" H 3200 2250 50  0001 C CNN
	1    3200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2450 3500 2850
Connection ~ 3500 2450
Wire Wire Line
	3500 2850 3500 3250
Connection ~ 3500 2850
Wire Wire Line
	4250 2650 3200 2650
Wire Wire Line
	3200 2650 3200 2450
Connection ~ 3200 2650
Wire Wire Line
	3200 2250 3200 2450
Connection ~ 3200 2450
Wire Wire Line
	3500 2850 4750 2850
Connection ~ 4750 2850
Wire Wire Line
	3800 1600 5500 1600
Wire Wire Line
	5500 1600 5500 2650
Wire Wire Line
	5500 2650 5250 2650
Wire Wire Line
	4750 2050 4750 1800
$Comp
L Device:R R3
U 1 1 62B5E7E2
P 5350 2300
AR Path="/63239588/62B5E7E2" Ref="R3"  Part="1" 
AR Path="/6323C0DC/62B5E7E2" Ref="R5"  Part="1" 
AR Path="/6323F750/62B5E7E2" Ref="R7"  Part="1" 
AR Path="/632405C4/62B5E7E2" Ref="R9"  Part="1" 
F 0 "R3" V 5557 2300 50  0000 C CNN
F 1 "100K" V 5466 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5280 2300 50  0001 C CNN
F 3 "~" H 5350 2300 50  0001 C CNN
	1    5350 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR030
U 1 1 62B5EFD2
P 5350 1900
AR Path="/63239588/62B5EFD2" Ref="#PWR030"  Part="1" 
AR Path="/6323C0DC/62B5EFD2" Ref="#PWR039"  Part="1" 
AR Path="/6323F750/62B5EFD2" Ref="#PWR048"  Part="1" 
AR Path="/632405C4/62B5EFD2" Ref="#PWR057"  Part="1" 
F 0 "#PWR030" H 5350 1750 50  0001 C CNN
F 1 "+3V3" H 5365 2073 50  0000 C CNN
F 2 "" H 5350 1900 50  0001 C CNN
F 3 "" H 5350 1900 50  0001 C CNN
	1    5350 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 62B6009C
P 5650 1950
AR Path="/63239588/62B6009C" Ref="R4"  Part="1" 
AR Path="/6323C0DC/62B6009C" Ref="R6"  Part="1" 
AR Path="/6323F750/62B6009C" Ref="R8"  Part="1" 
AR Path="/632405C4/62B6009C" Ref="R10"  Part="1" 
F 0 "R4" H 5720 1996 50  0000 L CNN
F 1 "220" H 5720 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5580 1950 50  0001 C CNN
F 3 "~" H 5650 1950 50  0001 C CNN
	1    5650 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2250 5650 2250
Wire Wire Line
	5650 2100 5650 2250
$Comp
L Device:LED D5
U 1 1 62B6106D
P 5650 1500
AR Path="/63239588/62B6106D" Ref="D5"  Part="1" 
AR Path="/6323C0DC/62B6106D" Ref="D6"  Part="1" 
AR Path="/6323F750/62B6106D" Ref="D7"  Part="1" 
AR Path="/632405C4/62B6106D" Ref="D8"  Part="1" 
F 0 "D5" V 5597 1580 50  0000 L CNN
F 1 "LED" V 5688 1580 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5650 1500 50  0001 C CNN
F 3 "~" H 5650 1500 50  0001 C CNN
	1    5650 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 1650 5650 1800
$Comp
L power:GND #PWR031
U 1 1 62B621FA
P 5650 1050
AR Path="/63239588/62B621FA" Ref="#PWR031"  Part="1" 
AR Path="/6323C0DC/62B621FA" Ref="#PWR040"  Part="1" 
AR Path="/6323F750/62B621FA" Ref="#PWR049"  Part="1" 
AR Path="/632405C4/62B621FA" Ref="#PWR058"  Part="1" 
F 0 "#PWR031" H 5650 800 50  0001 C CNN
F 1 "GND" H 5655 877 50  0000 C CNN
F 2 "" H 5650 1050 50  0001 C CNN
F 3 "" H 5650 1050 50  0001 C CNN
	1    5650 1050
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 1050 5650 1350
Text GLabel 5850 2250 2    50   Input ~ 0
SIGNAL
$Comp
L Connector:AudioJack2 J5
U 1 1 62B63065
P 6300 3350
AR Path="/63239588/62B63065" Ref="J5"  Part="1" 
AR Path="/6323C0DC/62B63065" Ref="J8"  Part="1" 
AR Path="/6323F750/62B63065" Ref="J11"  Part="1" 
AR Path="/632405C4/62B63065" Ref="J14"  Part="1" 
F 0 "J5" H 6120 3333 50  0000 R CNN
F 1 "PLANT" H 6120 3424 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_Ledino_KB3SPRS_Horizontal" H 6300 3350 50  0001 C CNN
F 3 "~" H 6300 3350 50  0001 C CNN
	1    6300 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 62B65502
P 6350 3800
AR Path="/63239588/62B65502" Ref="J6"  Part="1" 
AR Path="/6323C0DC/62B65502" Ref="J9"  Part="1" 
AR Path="/6323F750/62B65502" Ref="J12"  Part="1" 
AR Path="/632405C4/62B65502" Ref="J15"  Part="1" 
F 0 "J6" H 6322 3682 50  0000 R CNN
F 1 "Conn_01x02_Male" H 6322 3773 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 6350 3800 50  0001 C CNN
F 3 "~" H 6350 3800 50  0001 C CNN
	1    6350 3800
	-1   0    0    1   
$EndComp
Text GLabel 5850 2450 2    50   Input ~ 0
DIS
Text GLabel 5850 2650 2    50   Input ~ 0
THR
Wire Wire Line
	5250 2450 5350 2450
Wire Wire Line
	5350 1900 5350 2150
Text GLabel 5850 3350 0    50   Input ~ 0
THR
Text GLabel 5850 3450 0    50   Input ~ 0
DIS
Wire Wire Line
	5850 3350 6100 3350
Wire Wire Line
	5850 3450 6100 3450
Text GLabel 5850 3700 0    50   Input ~ 0
THR
Text GLabel 5850 3800 0    50   Input ~ 0
DIS
Wire Wire Line
	5850 3700 6150 3700
Wire Wire Line
	5850 3800 6150 3800
$Comp
L Connector:Conn_01x03_Male J7
U 1 1 63146D13
P 6750 1650
AR Path="/63239588/63146D13" Ref="J7"  Part="1" 
AR Path="/6323C0DC/63146D13" Ref="J10"  Part="1" 
AR Path="/6323F750/63146D13" Ref="J13"  Part="1" 
AR Path="/632405C4/63146D13" Ref="J16"  Part="1" 
F 0 "J7" H 6722 1582 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6722 1673 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 6750 1650 50  0001 C CNN
F 3 "~" H 6750 1650 50  0001 C CNN
	1    6750 1650
	-1   0    0    1   
$EndComp
Text GLabel 6400 1550 0    50   Input ~ 0
SIGNAL
$Comp
L power:GND #PWR033
U 1 1 6314A0C5
P 6400 1750
AR Path="/63239588/6314A0C5" Ref="#PWR033"  Part="1" 
AR Path="/6323C0DC/6314A0C5" Ref="#PWR042"  Part="1" 
AR Path="/6323F750/6314A0C5" Ref="#PWR051"  Part="1" 
AR Path="/632405C4/6314A0C5" Ref="#PWR060"  Part="1" 
F 0 "#PWR033" H 6400 1500 50  0001 C CNN
F 1 "GND" H 6405 1577 50  0000 C CNN
F 2 "" H 6400 1750 50  0001 C CNN
F 3 "" H 6400 1750 50  0001 C CNN
	1    6400 1750
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR032
U 1 1 6314A490
P 6400 1650
AR Path="/63239588/6314A490" Ref="#PWR032"  Part="1" 
AR Path="/6323C0DC/6314A490" Ref="#PWR041"  Part="1" 
AR Path="/6323F750/6314A490" Ref="#PWR050"  Part="1" 
AR Path="/632405C4/6314A490" Ref="#PWR059"  Part="1" 
F 0 "#PWR032" H 6400 1500 50  0001 C CNN
F 1 "+3V3" H 6415 1823 50  0000 C CNN
F 2 "" H 6400 1650 50  0001 C CNN
F 3 "" H 6400 1650 50  0001 C CNN
	1    6400 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 1550 6550 1550
Wire Wire Line
	6400 1650 6550 1650
Wire Wire Line
	6400 1750 6550 1750
$Comp
L Device:C C4
U 1 1 6314C1DE
P 3650 1600
AR Path="/63239588/6314C1DE" Ref="C4"  Part="1" 
AR Path="/6323C0DC/6314C1DE" Ref="C9"  Part="1" 
AR Path="/6323F750/6314C1DE" Ref="C14"  Part="1" 
AR Path="/632405C4/6314C1DE" Ref="C19"  Part="1" 
F 0 "C4" V 3902 1600 50  0000 C CNN
F 1 "42nF" V 3811 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3688 1450 50  0001 C CNN
F 3 "~" H 3650 1600 50  0001 C CNN
	1    3650 1600
	0    -1   -1   0   
$EndComp
Connection ~ 3800 1600
Wire Wire Line
	3800 1600 3800 2050
Wire Wire Line
	3500 1600 3500 2050
$Comp
L Device:C C5
U 1 1 63153131
P 3650 2050
AR Path="/63239588/63153131" Ref="C5"  Part="1" 
AR Path="/6323C0DC/63153131" Ref="C10"  Part="1" 
AR Path="/6323F750/63153131" Ref="C15"  Part="1" 
AR Path="/632405C4/63153131" Ref="C20"  Part="1" 
F 0 "C5" V 3902 2050 50  0000 C CNN
F 1 "42nF" V 3811 2050 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3688 1900 50  0001 C CNN
F 3 "~" H 3650 2050 50  0001 C CNN
	1    3650 2050
	0    -1   -1   0   
$EndComp
Connection ~ 3500 2050
Wire Wire Line
	3500 2050 3500 2450
Connection ~ 3800 2050
Wire Wire Line
	3800 2050 3800 2250
Wire Wire Line
	3200 2650 3200 2850
$Comp
L Device:C C2
U 1 1 631538AD
P 3350 2850
AR Path="/63239588/631538AD" Ref="C2"  Part="1" 
AR Path="/6323C0DC/631538AD" Ref="C7"  Part="1" 
AR Path="/6323F750/631538AD" Ref="C12"  Part="1" 
AR Path="/632405C4/631538AD" Ref="C17"  Part="1" 
F 0 "C2" V 3602 2850 50  0000 C CNN
F 1 "0.1uF" V 3511 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3388 2700 50  0001 C CNN
F 3 "~" H 3350 2850 50  0001 C CNN
	1    3350 2850
	0    -1   -1   0   
$EndComp
Connection ~ 3200 2850
Wire Wire Line
	3200 2850 3200 3250
$Comp
L Device:C C3
U 1 1 63153FF6
P 3350 3250
AR Path="/63239588/63153FF6" Ref="C3"  Part="1" 
AR Path="/6323C0DC/63153FF6" Ref="C8"  Part="1" 
AR Path="/6323F750/63153FF6" Ref="C13"  Part="1" 
AR Path="/632405C4/63153FF6" Ref="C18"  Part="1" 
F 0 "C3" V 3602 3250 50  0000 C CNN
F 1 "0.1uF" V 3511 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3388 3100 50  0001 C CNN
F 3 "~" H 3350 3250 50  0001 C CNN
	1    3350 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 2250 5650 2250
Connection ~ 5650 2250
Wire Wire Line
	5850 2650 5500 2650
Connection ~ 5500 2650
Wire Wire Line
	5850 2450 5350 2450
Connection ~ 5350 2450
Text HLabel 7000 2150 2    50   Input ~ 0
SIGNAL_OUT
Text GLabel 6850 2150 0    50   Input ~ 0
SIGNAL
Text HLabel 7000 2250 2    50   Input ~ 0
3V3
Text HLabel 7000 2350 2    50   Input ~ 0
GND
$Comp
L power:GND #PWR035
U 1 1 6315876F
P 6850 2350
AR Path="/63239588/6315876F" Ref="#PWR035"  Part="1" 
AR Path="/6323C0DC/6315876F" Ref="#PWR044"  Part="1" 
AR Path="/6323F750/6315876F" Ref="#PWR053"  Part="1" 
AR Path="/632405C4/6315876F" Ref="#PWR062"  Part="1" 
F 0 "#PWR035" H 6850 2100 50  0001 C CNN
F 1 "GND" H 6855 2177 50  0000 C CNN
F 2 "" H 6850 2350 50  0001 C CNN
F 3 "" H 6850 2350 50  0001 C CNN
	1    6850 2350
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR034
U 1 1 63158A69
P 6850 2250
AR Path="/63239588/63158A69" Ref="#PWR034"  Part="1" 
AR Path="/6323C0DC/63158A69" Ref="#PWR043"  Part="1" 
AR Path="/6323F750/63158A69" Ref="#PWR052"  Part="1" 
AR Path="/632405C4/63158A69" Ref="#PWR061"  Part="1" 
F 0 "#PWR034" H 6850 2100 50  0001 C CNN
F 1 "+3V3" H 6865 2423 50  0000 C CNN
F 2 "" H 6850 2250 50  0001 C CNN
F 3 "" H 6850 2250 50  0001 C CNN
	1    6850 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 2150 7000 2150
Wire Wire Line
	6850 2250 7000 2250
Wire Wire Line
	6850 2350 7000 2350
$EndSCHEMATC
