# mutual_resonance

Sound generation from [BiodataSonificationBreadboardKit](https://github.com/electricityforprogress/BiodataSonificationBreadboardKit). Code is Encapsulated in PlantChannel class so multiple channels are possible.
To change the threshold of a plant (=MIDI channel) press the button for the channel, change the knob position and press the button again to set the threshold.

The code is written for ESP32 but should also work on other microcontrollers that support multiple interrupts. MIDI output is on Serial1 (TX Pin changed to GPIO5) and debug info on default Serial (USB).

More infos and pictures at [http://www.katharinagross.tv/mutualresonance/](http://www.katharinagross.tv/mutualresonance/)

## Hardware

A KiCAD PCB design for a board with ESP32-MINI-32-V1.3, 4 input channels (= 4 plants) and MIDI output is available at `hardware/mutual_resonance`. The encoder knob is optional and has no function at the moment. The pin header J1 on the backside is optional to connect an ESP32 JTAG debugger.

![](doc/pcb_render01.png)
![](doc/pcb_render02.png)
![](doc/pcb_image01.jpg)

## Tutorials

* How to setup the 555 timer stuff: https://github.com/electricityforprogress/BiodataSonificationBreadboardKit/blob/9b1064cb4f23ed0b710c741ef8dff320ebb755cb/BiodataBreadboardArduinoKit_v01.pdf

* How to setup MIDI on Linux: http://tedfelix.com/linux/linux-midi.html