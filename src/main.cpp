#include <Arduino.h>
#include <FastLED.h>
#include <Bounce2.h>
#include "PlantChannel.h"

#define INPUT0 26
#define INPUT1 33
#define INPUT2 25
#define INPUT3 32

#define MIDI_OUT 5

#define LED_STATUS 4
#define LED_STATUS_COUNT 4
#define LED_STATUS_BRIGHTNESS 100

#define KNOB_L 17
#define KNOB_R 18
#define KNOB_BTN 16

// function declarations
void IRAM_ATTR isr0();
void IRAM_ATTR isr1();
void IRAM_ATTR isr2();
void IRAM_ATTR isr3();
void IRAM_ATTR doEncoder();
void sendMidiCLLBACK(uint8_t channel, const MIDImessage& m);

// Knob
Button knob_btn;
uint8_t knob_pos = 0;
bool configMode = false;

// LED
CRGB led_status[LED_STATUS_COUNT];

// plants
PlantChannel plants[] = {
    PlantChannel(INPUT0, 1, sendMidiCLLBACK),
    PlantChannel(INPUT1, 2, sendMidiCLLBACK),
    PlantChannel(INPUT2, 3, sendMidiCLLBACK),
    PlantChannel(INPUT3, 4, sendMidiCLLBACK)
};

// setup
int setup_current = -1;
long millis_last = millis();
float thresh_prev = 0.0;

void setup(){
    Serial.begin(115200);
    Serial.println("start!");

    // init buttons
    Serial.println("Init buttons..");
    pinMode(KNOB_L, INPUT_PULLUP);
    pinMode(KNOB_R, INPUT_PULLUP);
    knob_btn.attach(KNOB_BTN, INPUT_PULLUP);
    knob_btn.interval(100);
    knob_btn.setPressedState(LOW);
    attachInterrupt(KNOB_R, doEncoder, CHANGE);

    // init LEDs
    FastLED.addLeds<NEOPIXEL, LED_STATUS>(led_status, LED_STATUS_COUNT);
    for(int i = 0; i < LED_STATUS_COUNT; i++) {
        led_status[i] = CHSV(100, 255, LED_STATUS_BRIGHTNESS);
        FastLED.show();
        delay(200);
    }
    for(int i = 0; i < LED_STATUS_COUNT; i++) {
        led_status[i] = CHSV(0, 255, 0);
    }
    FastLED.show();

    // setup MIDI serial
    // Serial1 is GPIO10 by default but this GPIO is also used for internal flash so 
    // we change it to a different GPIO
    Serial1.begin(31250, SERIAL_8N1, -1, MIDI_OUT); //initialize at MIDI rate
    // Serial1.begin(31250); //initialize at MIDI rate
    for(size_t i=0; i<4; i++){
        plants[i].setMIDISerial(Serial1);
    }

    // init inputs/ISR
    pinMode(INPUT0, INPUT_PULLUP);
    attachInterrupt(INPUT0, isr0, RISING);
    
    pinMode(INPUT1, INPUT_PULLUP);
    attachInterrupt(INPUT1, isr1, RISING);

    pinMode(INPUT2, INPUT_PULLUP);
    attachInterrupt(INPUT2, isr2, RISING);

    pinMode(INPUT3, INPUT_PULLUP);
    attachInterrupt(INPUT3, isr3, RISING);
}

void loop(){
    long current = millis();

    if(knob_btn.released()){
        configMode = !configMode;
        Serial.printf("Config Mode for INPUT %d\n", knob_pos);
        for(int i = 0; i < LED_STATUS_COUNT; i++) {
            if(i == knob_pos){
                led_status[i] = CHSV(120, 255, LED_STATUS_BRIGHTNESS);
            } else {
                led_status[i] = CRGB::Black;
            }
        }
        FastLED.show();
    }


    if(current - millis_last > 1000){
        Serial.printf("ISR Counter: %u %u %u %u\n", plants[0].getCount(), plants[1].getCount(), plants[2].getCount(), plants[3].getCount());
        // Serial.printf("Buttons L=%d R=%d BTN=%d\n", digitalRead(KNOB_L), digitalRead(KNOB_R),  digitalRead(KNOB_BTN));
        millis_last = current;
    }

    // analyze samples and send notes
    for(size_t i=0; i<4; i++){
        plants[i].update();
    }

    FastLED.show();
}

void sendMidiCLLBACK(uint8_t channel, const MIDImessage& m){
    Serial.printf("[%u] Callback: type=%u duration=%u period=%u value=%u velocity=%u\n", channel, m.type, m.duration, m.period, m.value, m.velocity);
    auto value = map(m.value, 0, 127, 0, 255);
    led_status[channel-1] = CHSV(value, 255, min(m.velocity, (uint8_t)LED_STATUS_BRIGHTNESS));
    FastLED.show();
}

/**************************
 * Interrupts
 * ***********************/
void IRAM_ATTR isr0() {
    plants[0].sample();
}

void IRAM_ATTR isr1() {
    plants[1].sample();
}

void IRAM_ATTR isr2() {
    plants[2].sample();
}

void IRAM_ATTR isr3() {
    plants[3].sample();
}

void doEncoder()
{
  if (digitalRead(KNOB_L) == digitalRead(KNOB_R))
  {
	knob_pos++;
  }
  else
  {
	knob_pos--;
  }
  knob_pos % LED_STATUS_COUNT;
}