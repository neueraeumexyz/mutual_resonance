#include "PlantChannel.h"

PlantChannel::PlantChannel(uint8_t input, uint8_t channel, void (*cb)(uint8_t, const MIDImessage&)) : input(input), channel(channel), callback(cb)
{
    // // button
    // btn.attach(button, INPUT_PULLUP);
    // btn.interval(50);
    // btn.setPressedState(LOW);

    // // LED
    // pinMode(led, OUTPUT);
    // digitalWrite(led, HIGH);
    // delay(200);
    // digitalWrite(led, LOW);
}

PlantChannel::~PlantChannel()
{
}

void PlantChannel::setMIDISerial(HardwareSerial &midi)
{
    this->midi = midi;
}

void PlantChannel::update()
{
    currentMillis = millis();

    if (index >= SAMPLESIZE)
    {
        analyzeSample(); //if samples array full, also checked in analyzeSample(), call sample analysis
    }
    checkNote();    //turn off expired notes
    checkControl(); //update control value
}

void PlantChannel::sample()
{
    count++;
    if (index < SAMPLESIZE)
    {
        samples[index] = micros() - microseconds;
        microseconds = samples[index] + microseconds; //rebuild micros() value w/o recalling
        //micros() is very slow
        //try a higher precision counter
        //samples[index] = ((timer0_overflow_count << 8) + TCNT0) - microseconds;
        index++;
    }
}

void PlantChannel::analyzeSample()
{
    //eating up memory, one long at a time!
    unsigned long averg = 0;
    unsigned long maxim = 0;
    unsigned long minim = 100000;
    float stdevi = 0;
    unsigned long delta = 0;
    byte change = 0;

    if (index >= SAMPLESIZE)
    { //array is full
        unsigned long sampanalysis[ANALYSIZE];
        for (byte i = 0; i < ANALYSIZE; i++)
        {
            //skip first element in the array
            sampanalysis[i] = samples[i + 1]; //load analysis table (due to volitle)
            //manual calculation
            if (sampanalysis[i] > maxim)
            {
                maxim = sampanalysis[i];
            }
            if (sampanalysis[i] < minim)
            {
                minim = sampanalysis[i];
            }
            averg += sampanalysis[i];
            stdevi += sampanalysis[i] * sampanalysis[i]; //prep stdevi
        }

        //manual calculation
        averg = averg / ANALYSIZE;
        stdevi = sqrt(stdevi / ANALYSIZE - averg * averg); //calculate stdevu
        if (stdevi < 1)
        {
            stdevi = 1.0;
        } //min stdevi of 1
        delta = maxim - minim;

        //**********perform change detection
        if (delta > (stdevi * threshold))
        {
            change = 1;
        }
        //*********

        if (change)
        {                                                          // set note and control vector
            int dur = 150 + (map(delta % 127, 1, 127, 100, 2500)); //length of note
            int ramp = 3 + (dur % 100);                            //control slide rate, min 25 (or 3 ;)

            //set scaling, root key, note
            int setnote = map(averg % 127, 1, 127, noteMin, noteMax); //derive note, min and max note
            setnote = scaleNote(setnote, scale[currScale], root);     //scale the note
            // setnote = setnote + root; // (apply root?)
            setNote(setnote, 100, dur, channel);

            //derive control parameters and set
            setControl(controlNumber, controlMessage.value, delta % 127, ramp); //set the ramp rate for the control
        }
        //reset array for next sample
        index = 0;
    }
}

uint32_t PlantChannel::getCount()
{
    return count;
}

void PlantChannel::setThreshold(int value, int min, int max)
{
    float t = mapfloat(value, min, max, threshMin, threshMax);
    Serial.printf("Set threshold from %.2f to %.2f\n", threshold, t);
    threshold = t;
}

void PlantChannel::printMIDIMessage(const MIDImessage &m)
{
    Serial.printf("[%u] MIDI: type: %u value: %u velocity: %u duration: %lu period: %lu channel: %u\n",
                  channel, m.type, m.value, m.velocity, m.duration, m.period, m.channel);
}

void PlantChannel::setNote(int value, int velocity, long duration, int notechannel)
{
    //find available note in array (velocity = 0);
    for (int i = 0; i < POLYPHONY; i++)
    {
        if (!noteArray[i].velocity)
        {
            //if velocity is 0, replace note in array
            noteArray[i].type = 0;
            noteArray[i].value = value;
            noteArray[i].velocity = velocity;
            noteArray[i].duration = currentMillis + duration;
            noteArray[i].channel = notechannel;

            sendMIDI(MIDI_NOTE, channel, value, velocity);
            
            if(callback){
                    callback(channel, noteArray[i]);
            }
            break;
        }
    }
}

int PlantChannel::scaleNote(int note, int scale[], int root)
{
    //input note mod 12 for scaling, note/12 octave
    //search array for nearest note, return scaled*octave
    int scaled = note % 12;
    int octave = note / 12;
    int scalesize = (scale[0]);
    //search entire array and return closest scaled note
    scaled = scaleSearch(scaled, scale, scalesize);
    scaled = (scaled + (12 * octave)) + root; //apply octave and root
    return scaled;
}

int PlantChannel::scaleSearch(int note, int scale[], int scalesize)
{
    for (byte i = 1; i < scalesize; i++)
    {
        if (note == scale[i])
        {
            return note;
        }
        else
        {
            if (note < scale[i])
            {
                return scale[i];
            }
        } //highest scale value less than or equal to note
          //otherwise continue search
    }
    //didn't find note and didn't pass note value, uh oh!
    return 6; //give arbitrary value rather than fail
}

void PlantChannel::setControl(int type, int value, int velocity, long duration)
{
    controlMessage.type = type;
    controlMessage.value = value;
    controlMessage.velocity = velocity;
    controlMessage.period = duration;
    controlMessage.duration = currentMillis + duration; //schedule for update cycle
}

void PlantChannel::sendMIDI(MIDIType type, int channel, int data1, int data2)
{
    //  Note type = 144
    //  Control type = 176
    // remove MSBs on data
    data1 &= 0x7F; //number
    data2 &= 0x7F; //velocity

    byte statusbyte = (type | ((channel - 1) & 0x0F));

    cli(); //block interrupts, probably unnecessary
    midi.write(statusbyte);
    midi.write(data1);
    midi.write(data2);
    sei(); //enable interrupts
}

void PlantChannel::checkControl()
{
    // need to make this a smooth slide transition, using high precision
    // distance is current minus goal
    signed int distance = controlMessage.velocity - controlMessage.value;
    //if still sliding
    if (distance != 0)
    {
        //check timing
        if (currentMillis > controlMessage.duration)
        {                                                                    //and duration expired
            controlMessage.duration = currentMillis + controlMessage.period; //extend duration
                                                                             //update value
            if (distance > 0)
            {
                controlMessage.value += 1;
            }
            else
            {
                controlMessage.value -= 1;
            }

            //send MIDI control message after ramp duration expires, on each increment
            sendMIDI(MIDI_CONTROL, channel, controlMessage.type, controlMessage.value);


            // if(callback){
            //     callback(channel, controlMessage);
            // }

            //send out control voltage message on pin 17, PB3, digital 11
            // if(controlVoltage) { if(distance > 0) { rampUp(controlLED, map(controlMessage.value, 0, 127, 0 , 255), 5); }
            //                                     else { rampDown(controlLED, map(controlMessage.value, 0, 127, 0 , 255), 5); }
            // }
        }
    }
}

void PlantChannel::checkNote()
{
    for (int i = 0; i < POLYPHONY; i++)
    {
        if (noteArray[i].velocity)
        {
            if (noteArray[i].duration <= currentMillis)
            {
                printMIDIMessage(noteArray[i]);
                //send noteOff for all notes with expired duration
                sendMIDI(MIDI_NOTE, channel, noteArray[i].value, 0);
                noteArray[i].velocity = 0;

                if(callback){
                    callback(channel, noteArray[i]);
                }
            }
        }
    }
}

float PlantChannel::mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
