#!/usr/bin/env python3
from midi import MidiConnector


DEVICE = "/dev/ttyUSB1"

if __name__ == "__main__":
    conn = MidiConnector(DEVICE, timeout=5)
    while True:
        try:
            msg = conn.read()
            print(msg)
        except KeyboardInterrupt:
            break

    conn.close()
