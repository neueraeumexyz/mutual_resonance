EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1050 900  1250 900 
U 63239588
F0 "SIGNAL0" 50
F1 "555port.sch" 50
F2 "SIGNAL_OUT" I R 2300 1000 50 
F3 "3V3" I R 2300 1150 50 
F4 "GND" I R 2300 1300 50 
$EndSheet
$Comp
L power:GND #PWR06
U 1 1 6323A4AC
P 2500 1300
F 0 "#PWR06" H 2500 1050 50  0001 C CNN
F 1 "GND" V 2505 1172 50  0000 R CNN
F 2 "" H 2500 1300 50  0001 C CNN
F 3 "" H 2500 1300 50  0001 C CNN
	1    2500 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 1300 2300 1300
Wire Wire Line
	2300 1150 2500 1150
Text GLabel 2550 1000 2    50   Input ~ 0
SIGNAL0
Wire Wire Line
	2550 1000 2300 1000
$Sheet
S 1050 2050 1250 900 
U 6323C0DC
F0 "SIGNAL1" 50
F1 "555port.sch" 50
F2 "SIGNAL_OUT" I R 2300 2150 50 
F3 "3V3" I R 2300 2300 50 
F4 "GND" I R 2300 2450 50 
$EndSheet
$Comp
L power:+3V3 #PWR07
U 1 1 6323C0E2
P 2500 2300
F 0 "#PWR07" H 2500 2150 50  0001 C CNN
F 1 "+3V3" V 2515 2428 50  0000 L CNN
F 2 "" H 2500 2300 50  0001 C CNN
F 3 "" H 2500 2300 50  0001 C CNN
	1    2500 2300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 6323C0EC
P 2500 2450
F 0 "#PWR08" H 2500 2200 50  0001 C CNN
F 1 "GND" V 2505 2322 50  0000 R CNN
F 2 "" H 2500 2450 50  0001 C CNN
F 3 "" H 2500 2450 50  0001 C CNN
	1    2500 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 2450 2300 2450
Wire Wire Line
	2300 2300 2500 2300
Text GLabel 2550 2150 2    50   Input ~ 0
SIGNAL1
Wire Wire Line
	2550 2150 2300 2150
$Sheet
S 1000 3250 1250 900 
U 6323F750
F0 "SIGNAL2" 50
F1 "555port.sch" 50
F2 "SIGNAL_OUT" I R 2250 3350 50 
F3 "3V3" I R 2250 3500 50 
F4 "GND" I R 2250 3650 50 
$EndSheet
$Comp
L power:+3V3 #PWR01
U 1 1 6323F756
P 2450 3500
F 0 "#PWR01" H 2450 3350 50  0001 C CNN
F 1 "+3V3" V 2465 3628 50  0000 L CNN
F 2 "" H 2450 3500 50  0001 C CNN
F 3 "" H 2450 3500 50  0001 C CNN
	1    2450 3500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6323F760
P 2450 3650
F 0 "#PWR02" H 2450 3400 50  0001 C CNN
F 1 "GND" V 2455 3522 50  0000 R CNN
F 2 "" H 2450 3650 50  0001 C CNN
F 3 "" H 2450 3650 50  0001 C CNN
	1    2450 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 3650 2250 3650
Wire Wire Line
	2250 3500 2450 3500
Text GLabel 2500 3350 2    50   Input ~ 0
SIGNAL2
Wire Wire Line
	2500 3350 2250 3350
$Sheet
S 1000 4450 1250 900 
U 632405C4
F0 "SIGNAL3" 50
F1 "555port.sch" 50
F2 "SIGNAL_OUT" I R 2250 4550 50 
F3 "3V3" I R 2250 4700 50 
F4 "GND" I R 2250 4850 50 
$EndSheet
$Comp
L power:+3V3 #PWR03
U 1 1 632405CA
P 2450 4700
F 0 "#PWR03" H 2450 4550 50  0001 C CNN
F 1 "+3V3" V 2465 4828 50  0000 L CNN
F 2 "" H 2450 4700 50  0001 C CNN
F 3 "" H 2450 4700 50  0001 C CNN
	1    2450 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 632405D4
P 2450 4850
F 0 "#PWR04" H 2450 4600 50  0001 C CNN
F 1 "GND" V 2455 4722 50  0000 R CNN
F 2 "" H 2450 4850 50  0001 C CNN
F 3 "" H 2450 4850 50  0001 C CNN
	1    2450 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 4850 2250 4850
Wire Wire Line
	2250 4700 2450 4700
Text GLabel 2500 4550 2    50   Input ~ 0
SIGNAL3
Wire Wire Line
	2500 4550 2250 4550
$Comp
L ESP32_mini:mini_esp32 U1
U 1 1 63242D4A
P 6400 1700
F 0 "U1" H 6425 1825 50  0000 C CNN
F 1 "mini_esp32" H 6425 1734 50  0000 C CNN
F 2 "pcb:ESP32_mini" H 6550 1800 50  0001 C CNN
F 3 "" H 6550 1800 50  0001 C CNN
	1    6400 1700
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D1
U 1 1 632444FD
P 4850 5600
F 0 "D1" H 5194 5646 50  0000 L CNN
F 1 "WS2812B" H 5194 5555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 4900 5300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 4950 5225 50  0001 L TNN
	1    4850 5600
	1    0    0    -1  
$EndComp
Text GLabel 5600 4100 0    50   Input ~ 0
SIGNAL0
Wire Wire Line
	5600 4100 5900 4100
Text GLabel 5600 3200 0    50   Input ~ 0
SIGNAL1
Text GLabel 5600 4000 0    50   Input ~ 0
SIGNAL2
Text GLabel 5600 3100 0    50   Input ~ 0
SIGNAL3
Wire Wire Line
	5600 4000 5900 4000
Wire Wire Line
	5600 3200 5900 3200
Wire Wire Line
	5600 3100 5900 3100
$Comp
L power:+3V3 #PWR05
U 1 1 6323996F
P 2500 1150
F 0 "#PWR05" H 2500 1000 50  0001 C CNN
F 1 "+3V3" V 2515 1278 50  0000 L CNN
F 2 "" H 2500 1150 50  0001 C CNN
F 3 "" H 2500 1150 50  0001 C CNN
	1    2500 1150
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 632471E5
P 7200 1900
F 0 "#PWR017" H 7200 1750 50  0001 C CNN
F 1 "+3V3" V 7215 2028 50  0000 L CNN
F 2 "" H 7200 1900 50  0001 C CNN
F 3 "" H 7200 1900 50  0001 C CNN
	1    7200 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 1900 7200 1900
$Comp
L power:GND #PWR019
U 1 1 63247B69
P 7200 2100
F 0 "#PWR019" H 7200 1850 50  0001 C CNN
F 1 "GND" V 7205 1972 50  0000 R CNN
F 2 "" H 7200 2100 50  0001 C CNN
F 3 "" H 7200 2100 50  0001 C CNN
	1    7200 2100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 63247E68
P 7200 2200
F 0 "#PWR020" H 7200 1950 50  0001 C CNN
F 1 "GND" V 7205 2072 50  0000 R CNN
F 2 "" H 7200 2200 50  0001 C CNN
F 3 "" H 7200 2200 50  0001 C CNN
	1    7200 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 2000 7200 2000
Wire Wire Line
	6950 2100 7200 2100
Wire Wire Line
	6950 2200 7200 2200
Text GLabel 5550 2000 0    50   Input ~ 0
LED
Wire Wire Line
	5550 2000 5900 2000
$Comp
L power:+3V3 #PWR014
U 1 1 63249E43
P 4850 5100
F 0 "#PWR014" H 4850 4950 50  0001 C CNN
F 1 "+3V3" H 4865 5273 50  0000 C CNN
F 2 "" H 4850 5100 50  0001 C CNN
F 3 "" H 4850 5100 50  0001 C CNN
	1    4850 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 6324A4FA
P 4850 6050
F 0 "#PWR015" H 4850 5800 50  0001 C CNN
F 1 "GND" H 4855 5877 50  0000 C CNN
F 2 "" H 4850 6050 50  0001 C CNN
F 3 "" H 4850 6050 50  0001 C CNN
	1    4850 6050
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D2
U 1 1 6324ABE5
P 5600 5600
F 0 "D2" H 5944 5646 50  0000 L CNN
F 1 "WS2812B" H 5944 5555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5650 5300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5700 5225 50  0001 L TNN
	1    5600 5600
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D3
U 1 1 6324B3BB
P 6350 5600
F 0 "D3" H 6694 5646 50  0000 L CNN
F 1 "WS2812B" H 6694 5555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 6400 5300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 6450 5225 50  0001 L TNN
	1    6350 5600
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D4
U 1 1 6324B880
P 7100 5600
F 0 "D4" H 7444 5646 50  0000 L CNN
F 1 "WS2812B" H 7444 5555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 7150 5300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 7200 5225 50  0001 L TNN
	1    7100 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5600 5300 5600
Wire Wire Line
	5900 5600 6050 5600
Wire Wire Line
	6650 5600 6800 5600
Text GLabel 4050 5600 0    50   Input ~ 0
LED
$Comp
L Device:R R1
U 1 1 6324D442
P 4300 5600
F 0 "R1" V 4093 5600 50  0000 C CNN
F 1 "500" V 4184 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4230 5600 50  0001 C CNN
F 3 "~" H 4300 5600 50  0001 C CNN
	1    4300 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 5600 4150 5600
Wire Wire Line
	4450 5600 4550 5600
Wire Wire Line
	4850 5100 4850 5300
Wire Wire Line
	4850 6050 4850 5900
Wire Wire Line
	4850 5900 5600 5900
Connection ~ 4850 5900
Wire Wire Line
	5600 5900 6350 5900
Connection ~ 5600 5900
Wire Wire Line
	6350 5900 7100 5900
Connection ~ 6350 5900
Wire Wire Line
	7100 5300 6350 5300
Wire Wire Line
	6350 5300 5600 5300
Connection ~ 6350 5300
Wire Wire Line
	5600 5300 4850 5300
Connection ~ 5600 5300
Connection ~ 4850 5300
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 63255EFD
P 8600 4800
F 0 "SW1" H 8600 5167 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 8600 5076 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC12E-Switch_Vertical_H20mm" H 8450 4960 50  0001 C CNN
F 3 "~" H 8600 5060 50  0001 C CNN
	1    8600 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 63256A62
P 9050 4900
F 0 "#PWR022" H 9050 4650 50  0001 C CNN
F 1 "GND" V 9055 4772 50  0000 R CNN
F 2 "" H 9050 4900 50  0001 C CNN
F 3 "" H 9050 4900 50  0001 C CNN
	1    9050 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9050 4900 8900 4900
Text GLabel 5550 2700 0    50   Input ~ 0
BTN
Wire Wire Line
	5550 2700 5900 2700
Text GLabel 9100 4700 2    50   Input ~ 0
BTN
Wire Wire Line
	9100 4700 8900 4700
$Comp
L power:GND #PWR021
U 1 1 63259F07
P 8100 4800
F 0 "#PWR021" H 8100 4550 50  0001 C CNN
F 1 "GND" V 8105 4672 50  0000 R CNN
F 2 "" H 8100 4800 50  0001 C CNN
F 3 "" H 8100 4800 50  0001 C CNN
	1    8100 4800
	0    1    1    0   
$EndComp
Text GLabel 5550 2800 0    50   Input ~ 0
ROT0
Text GLabel 5550 2900 0    50   Input ~ 0
ROT1
Wire Wire Line
	5550 2800 5900 2800
Wire Wire Line
	5900 2900 5550 2900
Text GLabel 8100 4700 0    50   Input ~ 0
ROT0
Text GLabel 8100 4900 0    50   Input ~ 0
ROT1
Wire Wire Line
	8100 4900 8300 4900
Wire Wire Line
	8300 4800 8100 4800
Wire Wire Line
	8100 4700 8300 4700
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6326012E
P 4300 1250
F 0 "J1" H 4350 1667 50  0000 C CNN
F 1 "Debug" H 4350 1576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4300 1250 50  0001 C CNN
F 3 "~" H 4300 1250 50  0001 C CNN
	1    4300 1250
	1    0    0    -1  
$EndComp
Text GLabel 5550 2400 0    50   Input ~ 0
TDI
Text GLabel 5550 2500 0    50   Input ~ 0
TCK
Text GLabel 5550 2600 0    50   Input ~ 0
TMS
Wire Wire Line
	5550 2400 5900 2400
Wire Wire Line
	5550 2500 5900 2500
Wire Wire Line
	5550 2600 5900 2600
Text GLabel 7250 3400 2    50   Input ~ 0
TDO
Wire Wire Line
	7250 3400 6950 3400
Text GLabel 4800 1250 2    50   Input ~ 0
TDO
Text GLabel 4800 1350 2    50   Input ~ 0
TDI
Text GLabel 4800 1150 2    50   Input ~ 0
TCK
Text GLabel 4800 1050 2    50   Input ~ 0
TMS
$Comp
L power:+3V3 #PWR09
U 1 1 6326683F
P 3850 1050
F 0 "#PWR09" H 3850 900 50  0001 C CNN
F 1 "+3V3" V 3865 1178 50  0000 L CNN
F 2 "" H 3850 1050 50  0001 C CNN
F 3 "" H 3850 1050 50  0001 C CNN
	1    3850 1050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 63246403
P 7200 2000
F 0 "#PWR018" H 7200 1750 50  0001 C CNN
F 1 "GND" V 7205 1872 50  0000 R CNN
F 2 "" H 7200 2000 50  0001 C CNN
F 3 "" H 7200 2000 50  0001 C CNN
	1    7200 2000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 63267FD5
P 3850 1150
F 0 "#PWR010" H 3850 900 50  0001 C CNN
F 1 "GND" V 3855 1022 50  0000 R CNN
F 2 "" H 3850 1150 50  0001 C CNN
F 3 "" H 3850 1150 50  0001 C CNN
	1    3850 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 1050 4100 1050
Wire Wire Line
	3850 1150 4100 1150
Wire Wire Line
	4800 1050 4600 1050
Wire Wire Line
	4600 1150 4800 1150
Wire Wire Line
	4800 1250 4600 1250
Wire Wire Line
	4600 1350 4800 1350
$Comp
L power:GND #PWR011
U 1 1 6326FE97
P 3850 1250
F 0 "#PWR011" H 3850 1000 50  0001 C CNN
F 1 "GND" V 3855 1122 50  0000 R CNN
F 2 "" H 3850 1250 50  0001 C CNN
F 3 "" H 3850 1250 50  0001 C CNN
	1    3850 1250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 63270073
P 3850 1350
F 0 "#PWR012" H 3850 1100 50  0001 C CNN
F 1 "GND" V 3855 1222 50  0000 R CNN
F 2 "" H 3850 1350 50  0001 C CNN
F 3 "" H 3850 1350 50  0001 C CNN
	1    3850 1350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 63270256
P 3850 1450
F 0 "#PWR013" H 3850 1200 50  0001 C CNN
F 1 "GND" V 3855 1322 50  0000 R CNN
F 2 "" H 3850 1450 50  0001 C CNN
F 3 "" H 3850 1450 50  0001 C CNN
	1    3850 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 1250 4100 1250
Wire Wire Line
	4100 1350 3850 1350
Wire Wire Line
	3850 1450 4100 1450
Wire Notes Line
	3450 700  5100 700 
Wire Notes Line
	5100 700  5100 1600
Wire Notes Line
	5100 1600 3450 1600
Wire Notes Line
	3450 1600 3450 700 
Text Notes 3450 800  0    50   ~ 0
Debug Port
Text GLabel 5600 3700 0    50   Input ~ 0
SDA
Text GLabel 5600 3800 0    50   Input ~ 0
SCL
Wire Wire Line
	5600 3800 5900 3800
Wire Wire Line
	5600 3700 5900 3700
$Comp
L Connector:DIN-5 J4
U 1 1 6327B4CB
P 9250 3050
F 0 "J4" H 9250 2775 50  0000 C CNN
F 1 "MIDI OUT" H 9250 2684 50  0000 C CNN
F 2 "Eurocad:MIDI_DIN5" H 9250 3050 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 9250 3050 50  0001 C CNN
	1    9250 3050
	1    0    0    -1  
$EndComp
Text GLabel 5550 2200 0    50   Input ~ 0
MIDI_OUT
Wire Wire Line
	5550 2200 5900 2200
Text GLabel 9700 3150 2    50   Input ~ 0
MIDI_OUT
Wire Wire Line
	9700 3150 9600 3150
$Comp
L power:+5V #PWR016
U 1 1 6327FF74
P 7200 1800
F 0 "#PWR016" H 7200 1650 50  0001 C CNN
F 1 "+5V" V 7215 1928 50  0000 L CNN
F 2 "" H 7200 1800 50  0001 C CNN
F 3 "" H 7200 1800 50  0001 C CNN
	1    7200 1800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR026
U 1 1 63280554
P 10100 2950
F 0 "#PWR026" H 10100 2800 50  0001 C CNN
F 1 "+5V" V 10115 3078 50  0000 L CNN
F 2 "" H 10100 2950 50  0001 C CNN
F 3 "" H 10100 2950 50  0001 C CNN
	1    10100 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 1800 6950 1800
$Comp
L Device:R R2
U 1 1 6328521D
P 9800 2950
F 0 "R2" V 9593 2950 50  0000 C CNN
F 1 "220" V 9684 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9730 2950 50  0001 C CNN
F 3 "~" H 9800 2950 50  0001 C CNN
	1    9800 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 2950 9550 2950
Wire Wire Line
	9950 2950 10100 2950
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 6324CA93
P 9250 2500
F 0 "J3" H 9168 2175 50  0000 C CNN
F 1 "MIDI OUT" H 9168 2266 50  0000 C CNN
F 2 "Eurocad:MIDI_DIN5" H 9250 2500 50  0001 C CNN
F 3 "~" H 9250 2500 50  0001 C CNN
	1    9250 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 2500 9550 2500
Wire Wire Line
	9550 2500 9550 2950
Connection ~ 9550 2950
Wire Wire Line
	9450 2400 9600 2400
Wire Wire Line
	9600 2400 9600 3150
Connection ~ 9600 3150
Wire Wire Line
	9600 3150 9550 3150
Text GLabel 9650 1750 2    50   Input ~ 0
SDA
Text GLabel 9650 1850 2    50   Input ~ 0
SCL
Wire Wire Line
	9650 1850 9450 1850
Wire Wire Line
	9450 1750 9650 1750
$Comp
L Connector_Generic:Conn_01x05 J2
U 1 1 6325B1F3
P 9250 1650
F 0 "J2" H 9168 1225 50  0000 C CNN
F 1 "I2C" H 9168 1316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 9250 1650 50  0001 C CNN
F 3 "~" H 9250 1650 50  0001 C CNN
	1    9250 1650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 6325C1AA
P 9650 1650
F 0 "#PWR025" H 9650 1400 50  0001 C CNN
F 1 "GND" V 9655 1522 50  0000 R CNN
F 2 "" H 9650 1650 50  0001 C CNN
F 3 "" H 9650 1650 50  0001 C CNN
	1    9650 1650
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR024
U 1 1 6325C6A5
P 9650 1550
F 0 "#PWR024" H 9650 1400 50  0001 C CNN
F 1 "+3V3" V 9665 1678 50  0000 L CNN
F 2 "" H 9650 1550 50  0001 C CNN
F 3 "" H 9650 1550 50  0001 C CNN
	1    9650 1550
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR023
U 1 1 6325CBAA
P 9650 1450
F 0 "#PWR023" H 9650 1300 50  0001 C CNN
F 1 "+5V" V 9665 1578 50  0000 L CNN
F 2 "" H 9650 1450 50  0001 C CNN
F 3 "" H 9650 1450 50  0001 C CNN
	1    9650 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 1450 9450 1450
Wire Wire Line
	9450 1550 9650 1550
Wire Wire Line
	9650 1650 9450 1650
$EndSCHEMATC
